var app = {

    //PROPERTIES
    blocs: ["urgencies", "economia", "events", "local", "relacions", "varis"],
    blocsEl: [],
    torns: [],
    tornAvuiM: null,
    tornAvuiT: null,
    inputTorns: [],
    envTorns: null,
    toTorns: null,
    toODDVw: null,
    addTorns: null,
    editTorns: null,
    inputTornsView: null,
    ODDView: null,
    addPoint: null,
    points: {
        urgencies: [],
        relacions: [],
        varis: [],
        local: [],
        economia: [],
        events: [],
    },
    minutes: [],
    toInit: null,
    toInitODD: null,
    blocSel: null,
    inPoint: null,
    sendPointBtn: null,
    index: null,
    avisBloc: null,
    delTot: null,
    actesVw: null,
    toInitActes: null,
    toActesVw: null,
    saveMinutesBtn: null,
    inputActes: null,
    actesList: null,

    //FUNCTIONS

    //initialize app
    init: () => {
        //fill properties
        app.avisBloc = document.getElementById("avisSelect");
        app.index = document.getElementById("indexVw");
        app.toInit = document.getElementById("toInit");
        app.toInitODD = document.getElementById("toInitODD");
        app.tornAvuiM = document.getElementById("tMati");
        app.tornAvuiT = document.getElementById("tTarde");
        app.inputTorns = document.getElementsByClassName("tornsInput");
        app.envTorns = document.getElementById("envTorns");
        app.addTorns = document.getElementById("addTorns");
        app.sendPointBtn = document.getElementById("sendPoint");
        app.toTorns = document.getElementById("toTornsVw");
        app.inputTornsView = document.getElementById("inputTornsVw");
        app.editTorns = document.getElementById("editTorns");
        // app.inputODDView = document.getElementById("inputODDVw");
        app.addPoint = document.getElementById("addPoint");
        // app.pointsDiv = document.getElementById("points");
        app.blocsEl.push(document.getElementById("urgencies"));
        app.blocsEl.push(document.getElementById("economia"));
        app.blocsEl.push(document.getElementById("events"));
        app.blocsEl.push(document.getElementById("local"));
        app.blocsEl.push(document.getElementById("relacions"));
        app.blocsEl.push(document.getElementById("varis"));
        app.toODDVw = document.getElementById("toODDVw");
        app.ODDView = document.getElementById("ODDVw");
        app.blocSel = document.getElementById("bloc");
        app.inPoint = document.getElementById("inputPoint");
        app.delTot = document.getElementById("delTot");
        app.actesVw = document.getElementById("actes");
        app.toInitActes = document.getElementById("toInitActes");
        app.toActesVw = document.getElementById("toActes");
        app.saveMinutesBtn = document.getElementById("saveMinutes");
        app.inputActes = document.getElementById("inputActes");
        app.actesList = document.getElementById("actesList");

        app.delTot.onclick = () => {
             //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    deleteAll: true,
                },
            },
        );
        app.backToInit();
        };
        app.saveMinutesBtn.onclick = app.sendMinutes;
        app.toActesVw.onclick = app.toActes;
        app.toInitActes.onclick = app.backToInit;
        app.sendPointBtn.onclick = app.sendPoint;
        app.toODDVw.onclick = app.toODD;
        app.toInit.onclick = app.backToInit;
        app.toInitODD.onclick = app.backToInit;
        app.editTorns.onclick = app.toEditTorns;
        app.envTorns.onclick = app.send;
        app.toTorns.onclick = app.toTornsView;

        //listen to updates
        window.webxdc.setUpdateListener(function (update) {
            if (update.payload.torns) { //torns update
                app.torns = update.payload.torns;
                for (let i = 0; i < update.payload.torns.length; i++) {
                    app.inputTorns[i].value = update.payload.torns[i];
                }
                app.showTorns();
            } else if (update.payload.deletion) {
                app.deletePoint(update.payload.deletion);
            } else if (update.payload.punt) {
                app.points[update.payload.bloc].push(update.payload.punt);
                app.refreshODD();
            } else if(update.payload.deleteAll){
                app.deleteAllPoints();
            } else if(update.payload.link){
                app.minutes.unshift(update.payload);
                app.buildMinutes();
            }
        });
    },

    getTornsInput: () => {
        let torns = [];
        for (let i = 0; i < app.inputTorns.length; i++) {
            torns[i] = app.inputTorns[i].value;
        }
        return torns;
    },

    showTorns: () => {
        let date = new Date();
        switch (date.getDay()) {
            case 0:
                app.tornAvuiM.innerHTML = "Hoy estamos cerrados!";
                app.tornAvuiT.innerHTML = "";
                break;
            case 1:
                app.tornAvuiM.innerHTML = app.torns[0];
                app.tornAvuiT.innerHTML = app.torns[1];
                break;
            case 2:
                app.tornAvuiM.innerHTML = app.torns[2];
                app.tornAvuiT.innerHTML = app.torns[3];
                break;
            case 3:
                app.tornAvuiM.innerHTML = app.torns[4];
                app.tornAvuiT.innerHTML = app.torns[5];
                break;
            case 4:
                app.tornAvuiM.innerHTML = app.torns[6];
                app.tornAvuiT.innerHTML = app.torns[7];
                break;
            case 5:
                app.tornAvuiM.innerHTML = app.torns[8];
                app.tornAvuiT.innerHTML = app.torns[9];
                break;
            case 6:
                app.tornAvuiM.innerHTML = app.torns[10];
                app.tornAvuiT.innerHTML = app.torns[11];
        }
    },

    //send torns
    send: () => {
        let torns = app.getTornsInput();
        let info = window.webxdc.selfName + " ha afegit nous torns";

        //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    torns: torns,
                },
                info,
            },
            info
        );

        app.backToInit();
    },

    sendMinutes: () => {
        if(app.inputActes != ""){
            let date = new Date(Date.now());
            date = date.toDateString();
            let link = app.inputActes.value;
            let info = window.webxdc.selfName + " ha guardat un acta del " + date;

             //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    date: date,
                    link: link,
                },
                info,
            },
            info
        );
        }

    },

    buildMinutes: () => {
        app.actesList.innerHTML="";
        for(const acta of app.minutes){
            var link = document.createElement("a");
            link.setAttribute("href",acta.link);
            var li = document.createElement("li");
            li.appendChild(link);
            link.innerText = acta.date;
            actesList.appendChild(li);
        }
    },

    sendPoint: () => {
        if (app.blocSel.value == "none") {
            app.avisBloc.classList.remove("ninja");
            return;
        }

        let bloc = app.blocSel.value;
        app.blocSel.value = "none";
        let point = app.inPoint.value;
        app.inPoint.value = "";
        let info = window.webxdc.selfName + " ha afegit punts al ODD";
        app.avisBloc.classList.add("ninja");

        //send update
        window.webxdc.sendUpdate(
            {
                payload: {
                    punt: point,
                    bloc: bloc,
                },
                info,
            },
            info
        );
    },

    deletePoint: (deletion) => {
        var data = deletion.split("-");

        app.points[data[0]].splice(data[1],1);
        app.refreshODD();
    },

    deleteAllPoints: () => {
        app.points = {
            urgencies: [],
            relacions: [],
            varis: [],
            local: [],
            economia: [],
            events: [],
        };
        app.refreshODD();
    },

    refreshODD: () => {
        let childrenPoints;
        for (var i = 0; i < app.blocs.length; i++) {
            childrenPoints = [];
            app.points[app.blocs[i]].forEach((el, index) => {
                var deletion = app.blocs[i] + "-" + index;
                var point = document.createElement("li");
                point.setAttribute("id", app.blocs[i] + "-" + index);
                var span = document.createElement("span");
                span.classList.add("deltag");
                span.textContent = "X";
                span.onclick = () => {
                    let info = window.webxdc.selfName + " ha esborrat un punt del ODD";
                    //send update
                    window.webxdc.sendUpdate(
                        {
                            payload: {
                                deletion: deletion,
                            },
                            info,
                        },
                        info
                    );
                },
                point.innerText = el;
                point.appendChild(span);
                childrenPoints.push(point);
            });
            app.blocsEl[i].replaceChildren(...childrenPoints);
        }
    },

    backToInit: () => {
        for (let i = 0; i < app.inputTorns.length; i++) {
            app.inputTorns[i].readOnly = false;
        }
        app.inputTornsView.classList.add("ninja");
        app.envTorns.classList.add("ninja");
        app.editTorns.classList.remove("ninja");
        app.inputTornsView.classList.add("uneditable");
        app.ODDView.classList.add("ninja");
        app.index.classList.remove("ninja");
        app.actesVw.classList.add("ninja");
    },

    switchInputEditable: () => {
        if (app.inputTorns[0].readOnly) {
            for (let i = 0; i < app.inputTorns.length; i++) {
                app.inputTorns[i].readOnly = false;
            }
        } else {
            for (let i = 0; i < app.inputTorns.length; i++) {
                app.inputTorns[i].readOnly = true;
            }
        }
    },

    toTornsView: () => {
        app.inputTornsView.classList.remove("ninja");
        app.index.classList.add("ninja");
        app.switchInputEditable();
    },

    toEditTorns: () => {
        app.switchInputEditable();
        app.envTorns.classList.remove("ninja");
        app.editTorns.classList.add("ninja");
        app.inputTornsView.classList.remove("uneditable");
    },

    toODD: () => {
        app.ODDView.classList.remove("ninja");
        app.index.classList.add("ninja");
    },

    toActes: () => {
        app.index.classList.add("ninja");
        app.actesVw.classList.remove("ninja");
    },
};
window.addEventListener("load", app.init);
